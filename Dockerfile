FROM nginx:latest
RUN rm -f /etc/nginx/conf.d/default.conf
COPY dist/giflo-adm /usr/share/nginx/html/
COPY nginx.conf /etc/nginx/conf.d/
COPY ser.key /etc/nginx/
COPY ser.pem /etc/nginx/
COPY nginx.conf /etc/nginx/
RUN ls -la /usr/share/nginx/html*
EXPOSE 80 443