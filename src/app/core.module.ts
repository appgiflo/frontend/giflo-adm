import { NgModule } from '@angular/core';
/* START MY SERVICES IMPORTS*/
// Do not edit this comment content, it will be overwritten in next Skaffolder generation


/* END MY SERVICES IMPORTS*/

import { HTTP_INTERCEPTORS } from '../../node_modules/@angular/common/http';
import { AESService } from './security/aes.service';
import { AuthGuard } from './security/auth.guard';
import { AuthInterceptor } from './security/auth.interceptor';
import { AuthenticationService } from './security/authentication.service';
import { NotificadorAppService } from './security/services/notificador.app.service';
import { SecurityService } from './security/services/security.service';
import { CatalogoService } from './services/catalogo.service';
import { CuponService } from './services/cupon.service';
import { EmpresaService } from './services/empresa.service';
import { MessageService } from './services/message.service';
import { ParametroService } from './services/parametro.service';
import { PerfilService } from './services/perfil.service';
import { PlanService } from './services/plan.service';
import { SuscripcionService } from './services/suscripcion.service';
import { UserService } from './services/user.service';
import { UsuarioPerfilService } from './services/usuario-perfil.service';
import { VariedadService } from './services/variedad.service';


@NgModule({
  imports: [],
  providers: [
    /* START PROVIDERS */
    // Do not edit this comment content, it will be overwritten in next Skaffolder generation
    CatalogoService,
    CuponService,
    EmpresaService,
    PerfilService,
    PlanService,
    SuscripcionService,
    UserService,
    UsuarioPerfilService,
    VariedadService,
    MessageService,
    /* END PROVIDERS */

    // SECURITY
    AuthGuard,
    AuthenticationService,
    AESService,
    SecurityService,
    ParametroService,
    NotificadorAppService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
  exports: [],
  declarations: [
  ]
})
export class CoreModule {
}
