import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class NotificadorAppService {
  private spinner = new BehaviorSubject<{ show: boolean; text?: string }>({ show: false, text: '' });
  spinnerObs$: Observable<{ show: boolean; text?: string }> = this.spinner.asObservable();

  constructor() { }

  /**
   * METODO PARA NOTIFICAR QUE SE DEBE MOSTRAR LA FOTO DEL ASISTENTE VIRTUAL
   * AUTOR: FREDI ROMAN
   */
  public show( text?: string) {
    this.spinner.next({ show: true, text: text });
  }
  public hide() {
    this.spinner.next({ show: false, text: '' });
  }
}
