import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map, shareReplay, tap } from 'rxjs/operators';
import { User } from 'src/app/domain/gifloadmin_db/user';
import { environment } from 'src/environments/environment';

/**
 * Manage rest API about security
 */
@Injectable()
export class SecurityService {
  contextUrl = environment.endpoint;
  idClient = environment.apiCredential.idClient;
  passClient = environment.apiCredential.passClient;
  constructor(
    private http: HttpClient,
  ) { }

  /**
   * Login by username and SHA-3 password
   *
   * @param {string} username username for login
   * @param {string} password password in SHA-3
   * @param {boolean} remember store token in local storage
   * @returns {Observable<User>} logged user
   */
  login(username: string, password: string, remember: boolean): Observable<User> {
    let token =
      'Basic ' + btoa(`${this.idClient}:${this.passClient}`);
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8').set('prueba', 'SI').set('Authorization', token);
    let params = new URLSearchParams();
    params.append('username', username);
    params.append('password', password);
    params.append('grant_type', 'password');
    return this.http.post<User>(this.contextUrl + '/security/oauth/token', params.toString(), { headers: headers })
      .pipe(
        catchError(this.handleError),
        tap(user => this.setSession(user.access_token)),
        tap(user => this.setRefreshSession(user)),
        tap(user => { if (remember) this.setLocal(user.access_token); }),
        map(user => user),
        shareReplay()
      );
  }

  /**
   * Verify JWT token
   *
   * @param {string} tokenRefresh JWT token to verify
   * @returns {Observable<any>} logged user or error message
   */
  refreshToken(tokenRefresh: string): Observable<User> {
    let token =
      'Basic ' + btoa(`${this.idClient}:${this.passClient}`);
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8').set('prueba', 'SI').set('Authorization', token);
    let params = new URLSearchParams();
    params.append('refresh_token', tokenRefresh);
    params.append('grant_type', 'refresh_token');
    return this.http.post<any>(this.contextUrl + '/security/oauth/token', params.toString(), { headers: headers })
      .pipe(
        catchError(this.handleError),
        tap(user => this.setSession(user.access_token)),
        tap(user => this.setRefreshSession(user)),
        map(user => user),
      );
  }

  /**
   * Change password of current user
   *
   * @param {string} passwordNew New password to set in SHA-3
   * @param {string} passwordOld Old password to check in SHA-3
   * @returns {Observable<void>} Success or error
   */
  changePassword(passwordNew: string, passwordOld: string): Observable<void> {
    return this.http
      .post<void>(this.contextUrl + '/changePassword', {
        passwordNew: passwordNew,
        passwordOld: passwordOld
      })
      .pipe(
        map(response => response)
      );
  }

  /**
   * Set token in sessionStorage
   *
   * @private
   * @param {*} token JWT token to set in sessionStorage
   */
  private setSession(token?: string) {
    sessionStorage.setItem('token', token!);
  }

  /**
   * Set token in sessionStorage
   *
   * @private
   * @param {*} refreshToken JWT token to set in sessionStorage
   */
  private setRefreshSession(user: User) {
    sessionStorage.setItem('refres_token', user.refresh_token!);
  }

  /**
   * Set token in localStorage
   *
   * @private
   * @param {*} token JWT token to set in localStorage
   */
  private setLocal(token?: string) {
    localStorage.setItem('token', token!);
  }
  handleError(e: any) {
    let errorMessage = '';
    if (e.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${e.error.message}`;
    } else if (e.error !==undefined) {
      // client-side error
      errorMessage = `Error Code: ${e.status}\nMessage: ${e.error.error_description}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${e.status}\nMessage: ${e.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}
