import { BehaviorSubject, Observable } from 'rxjs';
import { User } from 'src/app/domain/gifloadmin_db/user';
import * as _ from 'lodash';

/**
 * Store the current user
 */
class CurrentUser {
    private currentUserSubject = new BehaviorSubject(new User(-1,'','','',[]));
    public currentUser$: Observable<User> = this.currentUserSubject.asObservable();

    setUser(user: User) {
        this.currentUserSubject.next(_.cloneDeep(user));
    }
}

export const store = new CurrentUser();
