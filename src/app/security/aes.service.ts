import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';

/**
 * This service manage the Authentication
 */
@Injectable()
export class AESService {
  key: string = "07CElMzjold3WC4oqCZwZcy1maq/yxTOj3t0K2cb8k4=";
  iv: string = "8+zWLJamuU6AD9Bm6woWTg==";
  constructor(
  ) { }
  encryptUsingAES256(value:string) {
    let _key = CryptoJS.enc.Base64.parse(this.key);
    let _iv = CryptoJS.enc.Base64.parse(this.iv);
    let encrypted = CryptoJS.AES.encrypt(
      value, _key, {
      keySize: 16,
      iv: _iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7
    });
    var encryptedText = encrypted.toString();
    return encryptedText;
  }
  decryptUsingAES256(encrypt:string) {
    let _key = CryptoJS.enc.Utf8.parse(this.key);
    let _iv = CryptoJS.enc.Utf8.parse(this.iv);
    var decrypted = CryptoJS.AES.decrypt(
      encrypt, _key, {
      keySize: 16,
      iv: _iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7
    }).toString(CryptoJS.enc.Utf8);
    return decrypted;
  }

}
