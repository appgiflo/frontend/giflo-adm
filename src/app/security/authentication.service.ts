import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'src/app/domain/gifloadmin_db/user';
import { store } from 'src/app/security/current-user';
import { SecurityService } from 'src/app/security/services/security.service';
import { Router } from '@angular/router';

/**
 * This service manage the Authentication
 */
@Injectable()
export class AuthenticationService {
  constructor(
    private securityService: SecurityService,
    private router: Router,
  ) { }

  /**
   * Get the logged user
   */
  public getUser(): Observable<User> {
    return new Observable<User>((ob: any) => {
      // Get JWT token from browser storage
      // Get user from store
      store.currentUser$.subscribe(user => {
        const token = sessionStorage.getItem('token');
        const refreshToken = sessionStorage.getItem('refres_token');
        if (token && user.idUser !== -1) {
          // User logged and stored token
          ob.next(user);
        } else if (token && user.idUser == -1 && refreshToken) {
          // If refresh page and have token but not logged user
          // Verify token and get user
          this.securityService.refreshToken(refreshToken!).subscribe(
            usr => {
              ob.next(usr);
            },
            error => {
              ob.next(user);
            }
          );
        } else {
          // Logged user
          ob.next(user);
        }
      });
    });
  }

  /**
   * Logout function
   */
  logout() {
    // Clear token and remove user from local storage to log user logout
    localStorage.removeItem('token');
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('refres_token');
    store.setUser(new User(-1, '', '', '', []));
    this.router.navigate(['/login']);
  }

}
