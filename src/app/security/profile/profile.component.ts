// Import Libraries
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Location } from '@angular/common';


// Security
import { SecurityService } from '../services/security.service';
import { AuthenticationService } from '../authentication.service';
import { User } from 'src/app/domain/gifloadmin_db/user';
import { UserService } from 'src/app/services/user.service';
import { store } from 'src/app/security/current-user';

/**
 * Edit my profile
 */
@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
})
export class ProfileComponent implements OnInit {

    user!: User;
    passwordOld!: string;
    passwordNew!: string;
    passwordNewConfirm!: string;
    showError!: boolean;
    @ViewChild('closeModal')
    closeModal!: ElementRef;

    constructor(
        private userService: UserService,
        private authenticationService: AuthenticationService,
        private securityService: SecurityService,
        private router: Router,
        private route: ActivatedRoute,
    ) { }

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            // Get logged user
            this.authenticationService.getUser().subscribe(user => this.user = user);
        });
    }

    /**
     * Save User
     *
     * @param {boolean} valid Form validity
     */
    save(valid: boolean) {
        if (valid)
            this.userService.update(this.user).subscribe(data => {
                this.userService.get(this.user.idUser!).subscribe(user => {
                    store.setUser(user);
                    this.router.navigateByUrl('/home');
                });
            });
    }

    /**
     * Change password of user
     */
    changePassword() {
        this.showError = false;
        // Change password
        this.securityService.changePassword(this.passwordNew, this.passwordOld).subscribe(data => {
            this.passwordOld = '';
            this.passwordNew = '';
            this.passwordNewConfirm = '';
            this.showError = false;
            this.closeModal.nativeElement.click();
        }, err => {
            this.showError = true;
        });
    }
}

