import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

/**
 * This interceptor get token from sessionStorage if it is set and put the JWT token in the header Authorization var
 */
@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Get token
        let token = sessionStorage.getItem('token') || localStorage.getItem('token');
        if (req.headers.get('Authorization')===null && token){
            return next.handle(req.clone({
                setHeaders: {
                    'DeviceInfo': 'Web Admin',
                    'Authorization':'Bearer ' + token,
                    'Content-Type':'application/json;',
                    'Charset':'utf-8'
                },
            }));
        }else{
            return next.handle(req.clone({
                setHeaders: {
                    'DeviceInfo': 'Web Admin'
                }
            }));
        }
    }
}
