import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { User } from 'src/app/domain/gifloadmin_db/user';
import { UserService } from 'src/app/services/user.service';
import { AuthenticationService } from 'src/app/security/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ViewChild, ElementRef } from '@angular/core';

declare var jQuery: any;
/**
 * Edit user by Admin
 */
@Component({
    selector: 'app-manage-user-edit',
    templateUrl: './manage-user-edit.component.html',
})
export class ManageUserEditComponent implements OnInit {

    user!: User;
    passwordNew!: string;
    passwordNewConfirm!: string;
    passwordAdmin!: string;
    showError!: boolean;
    @ViewChild('closeModal')
    closeModal!: ElementRef;

    constructor(
        private userService: UserService,
        private authenticationService: AuthenticationService,
        private router: Router,
        private route: ActivatedRoute
    ) { }

    ngOnInit(): void {

        this.route.params.subscribe(params => {

            if (params.id === 'new') {
                // New User
                this.user =  new User(-1,'','','',[]);
            } else {
                // Get User
                this.userService.get(params.id).subscribe(user => this.user = user);
            }
        });
    }

    /**
     * Save or create User
     */
    save(): void {
        if (this.user.idUser) {
            // Save
            this.userService.update(this.user).subscribe(data => this.router.navigateByUrl('/manage-users'));
        } else {
            // Create
            this.user.contrasenia = this.user.contrasenia;
            this.userService.create(this.user).subscribe(data => this.router.navigateByUrl('/manage-users'));
        }
    }

    /**
     * Delete user
     */
    deleteUser(): void {
        this.userService.remove(this.user.idUser).subscribe(data => this.router.navigateByUrl('/manage-users'));
    }

    /**
     * Add roles to user
     *
     * @param {*} role Role to add
     */
    addRole(role: any): void {
        if (role.value) {
            this.user.perfiles.push(role.value);
            role.value = '';
        }
    }

    /**
     * Remove role from user
     *
     * @param {number} index Index of the role in the array
     */
    removeRole(index: number) {
        this.user.perfiles.splice(index, 1);
    }

    /**
     * Change user password
     */
    changePassword() {

        this.userService.changePassword(this.user.idUser, this.passwordNew, this.passwordAdmin).subscribe(data => {
            this.passwordAdmin = '';
            this.passwordNew = '';
            this.passwordNewConfirm = '';
            this.showError = false;
            this.closeModal.nativeElement.click();
        }, err => {
            this.showError = true;
        });
    }

    trackByFn(index: number, item: any) {
        return index;
    }
}
