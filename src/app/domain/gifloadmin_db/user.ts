import { UserBase } from './base/user.base';
import { Perfil } from './perfil';

/**
 * YOU CAN OVERRIDE HERE UserBase.ts
 */
export class User extends UserBase {

  // Insert here your custom attributes and function

  // Functions for User

  public access_token?: string;
  public refresh_token?: string;


  constructor(
    idUser: number,
    correo: string,
    access_token: string,
    refresh_token: string,
    perfiles: Perfil[]
  ) {
    super();
    this.idUser = idUser;
    this.correo = correo;
    this.access_token = access_token;
    this.refresh_token = refresh_token;
    this.perfiles = perfiles
  }

  // UTILS FUNCTIONS

  /**
   * Check if logged user is admin
   */
  isAdmin(): boolean {
    if (!this.perfiles)
      return false;
    return this.perfiles.find(p => p.nombre === 'ROLE_SUPERADMIN') !== -1;
  }

  /**
   * Check if logged user has one role
   */
  hasRole(role: string | Array<string>): boolean {
    if (!this.perfiles) return false;
    if (typeof role === 'string') {
      return (this.perfiles.find(p => p.nombre === role) !== -1);
    } else {
      const found = role.filter(rol => {
        var indice = this.perfiles.findIndex(p => p.nombre === rol);
        return indice !== -1;
      });
      return found.length > 0;
    }
  }
}
