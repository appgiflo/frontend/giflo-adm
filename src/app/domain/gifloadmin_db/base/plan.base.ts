/**
 *
 *
  _____                      _              _ _ _     _   _     _        __ _ _
 |  __ \                    | |            | (_) |   | | | |   (_)      / _(_) |
 | |  | | ___    _ __   ___ | |_    ___  __| |_| |_  | |_| |__  _ ___  | |_ _| | ___
 | |  | |/ _ \  | '_ \ / _ \| __|  / _ \/ _` | | __| | __| '_ \| / __| |  _| | |/ _ \
 | |__| | (_) | | | | | (_) | |_  |  __/ (_| | | |_  | |_| | | | \__ \ | | | | |  __/
 |_____/ \___/  |_| |_|\___/ \__|  \___|\__,_|_|\__|  \__|_| |_|_|___/ |_| |_|_|\___|


 * DO NOT EDIT THIS FILE!!
 *
 *  TO CUSTOMIZE PlanBase PLEASE EDIT ../plan.ts
 *
 *  -- THIS FILE WILL BE OVERWRITTEN ON THE NEXT SKAFFOLDER'S CODE GENERATION --
 *
 */
import { Cupon } from '../cupon';

/**
 * This is the model of Plan object
 *
 */
export class PlanBase {

    constructor() { }

    public id?: string;
    public colorPrimario?: string;
    public colorSecundario?: string;
    public colorTexto?: string;
    public estado?: string;
    public meses?: number;
    public nombre?: string;
    public valor?: number;
    public orden?: number;
    // Relations cupon
    public cupon?: Cupon | string;
}
