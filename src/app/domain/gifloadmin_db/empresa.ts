import { EmpresaBase } from './base/empresa.base';

/**
 * YOU CAN OVERRIDE HERE EmpresaBase.ts
 */
export class Empresa extends EmpresaBase {

    // Insert here your custom attributes and function
}
