import { PerfilBase } from './base/perfil.base';

/**
 * YOU CAN OVERRIDE HERE PerfilBase.ts
 */
export class Perfil extends PerfilBase {

    // Insert here your custom attributes and function
}
