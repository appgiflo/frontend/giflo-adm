import { NgModule } from '@angular/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDialogModule } from '@angular/material/dialog';
import { ToolbarButtonComponent } from '../components/toolbar-button/toolbar-button.component';
import { SearchPipe } from '../pipes/search.pipe';
import { DialogComponent } from '../components/dialog/dialog.component';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  imports: [MatToolbarModule, MatDialogModule,MatButtonModule],
  declarations: [
    SearchPipe,
    ToolbarButtonComponent, DialogComponent
  ],
  exports: [SearchPipe, ToolbarButtonComponent, DialogComponent]
})
export class SharedModule {
}
