// DEPENDENCIES
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/* START MY VIEWS IMPORT */
// Do not edit this comment content, it will be overwritten in next Skaffolder generation

import { AuthGuard } from './security/auth.guard';

/**
 * WEB APP ROUTES
 */
const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },

    /* START MY VIEWS */

    {
        path: 'catalogos/:id',
        loadChildren: () => import('./pages/catalogo-edit/catalogo-edit.module').then(m => m.CatalogoEditModule), canActivate: [AuthGuard], data: ["ROLE_SUPERADMIN"]
    },
    {
        path: 'catalogos',
        loadChildren: () => import('./pages/catalogo-list/catalogo-list.module').then(m => m.CatalogoListModule), canActivate: [AuthGuard], data: ["ROLE_SUPERADMIN"]
    },
    {
        path: 'parametros',
        loadChildren: () => import('./pages/parametro-list/parametro-list.module').then(m => m.ParametroListModule), canActivate: [AuthGuard], data: ["ROLE_SUPERADMIN"]
    },
    {
        path: 'parametros/:id',
        loadChildren: () => import('./pages/parametro-edit/parametro-edit.module').then(m => m.ParametroEditModule), canActivate: [AuthGuard], data: ["ROLE_SUPERADMIN"]
    },
    {
        path: 'cupons/:id',
        loadChildren: () => import('./pages/cupon-edit/cupon-edit.module').then(m => m.CuponEditModule), canActivate: [AuthGuard]
    },
    {
        path: 'cupons',
        loadChildren: () => import('./pages/cupon-list/cupon-list.module').then(m => m.CuponListModule), canActivate: [AuthGuard], data: ["ROLE_SUPERADMIN"]
    },
    {
        path: 'home',
        loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'plans/:id',
        loadChildren: () => import('./pages/plan-edit/plan-edit.module').then(m => m.PlanEditModule), canActivate: [AuthGuard], data: ["ROLE_SUPERADMIN"]
    },
    {
        path: 'plans',
        loadChildren: () => import('./pages/plan-list/plan-list.module').then(m => m.PlanListModule), canActivate: [AuthGuard], data: ["ROLE_SUPERADMIN"]
    },
    {
        path: 'suscripcions/:id',
        loadChildren: () => import('./pages/suscripcion-edit/suscripcion-edit.module').then(m => m.SuscripcionEditModule), canActivate: [AuthGuard], data: ["ROLE_SUPERADMIN"]
    },
    {
        path: 'suscripcions',
        loadChildren: () => import('./pages/suscripcion-list/suscripcion-list.module').then(m => m.SuscripcionListModule),
        canActivate: [AuthGuard], data: ["ROLE_SUPERADMIN"]
    },
    {
        path: 'variedads/:id',
        loadChildren: () => import('./pages/variedad-edit/variedad-edit.module').then(m => m.VariedadEditModule),
        canActivate: [AuthGuard], data: ["ROLE_SUPERADMIN"]
    },
    {
        path: 'variedads',
        loadChildren: () => import('./pages/variedad-list/variedad-list.module').then(m => m.VariedadListModule),
        canActivate: [AuthGuard], data: ["ROLE_SUPERADMIN"]
    },
    {
        path: 'message',
        loadChildren: () => import('./pages/message-edit/message-edit.module').then(m => m.MessageEditModule),
        canActivate: [AuthGuard], data: ["ROLE_SUPERADMIN"]
    },

    /* END MY VIEWS */

    // SECURITY
    {
        path: 'manage-users',
        loadChildren: () => import('./security/manage-user/list-user/manage-user-list.module').then(m => m.ManageUserListModule), canActivate: [AuthGuard], data: ["ROLE_SUPERADMIN"]
    },
    {
        path: 'manage-users/:id',
        loadChildren: () => import('./security/manage-user/edit-user/manage-user-edit.module').then(m => m.ManageUserEditModule), canActivate: [AuthGuard], data: ["ROLE_SUPERADMIN"]
    },
    {
        path: 'profile',
        loadChildren: () => import('./security/profile/profile.module').then(m => m.ProfileModule), canActivate: [AuthGuard]
    },
    {
        path: 'login',
        loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule)
    },
    { path: '**', redirectTo: '/home' ,pathMatch: 'full' }
];

/**
 * ROUTING MODULE
 */
@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true })],
    exports: [RouterModule]
})

export class AppRoutingModule { }
