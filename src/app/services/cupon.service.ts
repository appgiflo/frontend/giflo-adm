// BASE SERVICE
import { Injectable } from '@angular/core';
import { CuponBaseService } from './base/cupon.base.service';


// start documentation
/**
 * Custom APIs
 *
 */
// end documentation

/**
 * YOU CAN OVERRIDE HERE CuponBaseService
 */
 @Injectable()
export class CuponService extends CuponBaseService {

}
