// BASE SERVICE
import { Injectable } from '@angular/core';
import { EmpresaBaseService } from './base/empresa.base.service';


// start documentation
/**
 * Custom APIs
 *
 */
// end documentation

/**
 * YOU CAN OVERRIDE HERE EmpresaBaseService
 */
 @Injectable()
export class EmpresaService extends EmpresaBaseService {

}
