// BASE SERVICE
import { Injectable } from '@angular/core';
import { UsuarioPerfilBaseService } from './base/usuario-perfil.base.service';


// start documentation
/**
 * Custom APIs
 *
 */
// end documentation

/**
 * YOU CAN OVERRIDE HERE UsuarioPerfilBaseService
 */
 @Injectable()
export class UsuarioPerfilService extends UsuarioPerfilBaseService {

}
