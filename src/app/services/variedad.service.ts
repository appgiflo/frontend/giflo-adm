// BASE SERVICE
import { Injectable } from '@angular/core';
import { VariedadBaseService } from './base/variedad.base.service';


// start documentation
/**
 * Custom APIs
 *
 */
// end documentation

/**
 * YOU CAN OVERRIDE HERE VariedadBaseService
 */
 @Injectable()
export class VariedadService extends VariedadBaseService {

}
