// BASE SERVICE
import { Injectable } from '@angular/core';
import { PerfilBaseService } from './base/perfil.base.service';


// start documentation
/**
 * Custom APIs
 *
 */
// end documentation

/**
 * YOU CAN OVERRIDE HERE PerfilBaseService
 */
 @Injectable()
export class PerfilService extends PerfilBaseService {

}
