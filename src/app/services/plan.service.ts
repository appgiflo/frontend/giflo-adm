// BASE SERVICE
import { Injectable } from '@angular/core';
import { PlanBaseService } from './base/plan.base.service';


// start documentation
/**
 * Custom APIs
 *
 */
// end documentation

/**
 * YOU CAN OVERRIDE HERE PlanBaseService
 */
 @Injectable()
export class PlanService extends PlanBaseService {

}
