/**
 *
 *
  _____                      _              _ _ _     _   _     _        __ _ _
 |  __ \                    | |            | (_) |   | | | |   (_)      / _(_) |
 | |  | | ___    _ __   ___ | |_    ___  __| |_| |_  | |_| |__  _ ___  | |_ _| | ___
 | |  | |/ _ \  | '_ \ / _ \| __|  / _ \/ _` | | __| | __| '_ \| / __| |  _| | |/ _ \
 | |__| | (_) | | | | | (_) | |_  |  __/ (_| | | |_  | |_| | | | \__ \ | | | | |  __/
 |_____/ \___/  |_| |_|\___/ \__|  \___|\__,_|_|\__|  \__|_| |_|_|___/ |_| |_|_|\___|

 * DO NOT EDIT THIS FILE!!
 *
 *  FOR CUSTOMIZE planBaseService PLEASE EDIT ../plan.service.ts
 *
 *  -- THIS FILE WILL BE OVERWRITTEN ON THE NEXT SKAFFOLDER'S CODE GENERATION --
 *
 */
 // DEPENDENCIES
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

// CONFIG
import { environment } from '../../../environments/environment';

// MODEL
import { Plan } from '../../domain/gifloadmin_db/plan';

/**
 * THIS SERVICE MAKE HTTP REQUEST TO SERVER, FOR CUSTOMIZE IT EDIT ../Plan.service.ts
 */

/*
 * SCHEMA DB Plan
 *
	{
		colorPrimario: {
			type: 'String'
		},
		colorSecundario: {
			type: 'String'
		},
		colorTexto: {
			type: 'String'
		},
		estado: {
			type: 'String'
		},
		meses: {
			type: 'Integer'
		},
		nombre: {
			type: 'String'
		},
		valor: {
			type: 'Integer'
		},
		//RELATIONS
		//EXTERNAL RELATIONS
		cupon: {
			type: Schema.ObjectId,
			ref : "Plan"
		},
	}
 *
 */
@Injectable()
export class PlanBaseService {

    contextUrl: string = environment.endpoint + '/system/plan';
    constructor(
        protected http: HttpClient
        ) { }

    // CRUD METHODS

    /**
    * PlanService.create
    *   @description CRUD ACTION create
    *   @param Plan obj Object to insert
    *
    */
    create(item: Plan): Observable<Plan> {
        return this.http
            .post<Plan>(this.contextUrl, item)
            .pipe(map(data => data));
    }

    /**
    * PlanService.delete
    *   @description CRUD ACTION delete
    *   @param ObjectId id Id Plan
    *
    */
    remove(id: string): Observable<void> {
        return this.http
            .delete<void>(this.contextUrl + '/' + id)
            .pipe(map(data => data));
    }

    /**
    * PlanService.findBycupon
    *   @description CRUD ACTION findBycupon
    *   @param Objectid key Id of model to search for
    *
    */
    findByCupon(id: string): Observable<Plan[]> {
        return this.http
            .get<Plan[]>(this.contextUrl + '/findBycupon/' + id)
            .pipe(
                map(response => response)
            );
    }

    /**
    * PlanService.get
    *   @description CRUD ACTION get
    *   @param ObjectId id Id Plan
    *   @returns Plan
    *
    */
    get(id: string): Observable<Plan> {
        return this.http
            .get<Plan>(this.contextUrl + '/' + id)
            .pipe(map(data => data));
    }

    /**
    * PlanService.list
    *   @description CRUD ACTION list
    *   @returns ARRAY OF Plan
    *
    */
    list(): Observable<any> {
        return this.http
            .get<any>(this.contextUrl,{ params: { 'page': 0, 'size': 500 } });
    }

    /**
    * PlanService.update
    *   @description CRUD ACTION update
    *   @param ObjectId id Id Plan
    *   @returns Plan
    *
    */
    update(item: Plan): Observable<Plan> {
        return this.http
            .put<Plan>(this.contextUrl + '/' + item.id, item);
    }


    // Custom APIs

}
