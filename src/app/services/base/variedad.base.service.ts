
// DEPENDENCIES
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

// CONFIG
import { environment } from '../../../environments/environment';

// MODEL
import { Variedad } from '../../domain/gifloadmin_db/variedad';


@Injectable()
export class VariedadBaseService {

  contextUrl: string = environment.endpoint + '/system/';
  constructor(
    protected http: HttpClient
  ) { }


  /**
  * VariedadService.create
  *   @description CRUD ACTION create
  *   @param Variedad obj Object to insert
  *
  */
  save(item: Variedad): Observable<Variedad> {
    return this.http
      .post<Variedad>(this.contextUrl + 'rest/variedad/save', item)
      .pipe(map(data => data));
  }

  /**
  * VariedadService.delete
  *   @description CRUD ACTION delete
  *   @param ObjectId id Id Variedad
  *
  */
  remove(id: string): Observable<void> {
    return this.http
      .delete<void>(this.contextUrl + 'variedad/' + id)
      .pipe(map(data => data));
  }

  /**
  * VariedadService.get
  *   @description CRUD ACTION get
  *   @param ObjectId id Id Variedad
  *   @returns Variedad
  *
  */
  get(id: string): Observable<Variedad> {
    return this.http
      .get<Variedad>(this.contextUrl + 'variedad/search/findByIdCustom?idVariedad=' + id)
      .pipe(map(data => data));
  }

  /**
  * VariedadService.list
  *   @description CRUD ACTION list
  *   @returns ARRAY OF Variedad
  *
  */
  list(): Observable<Variedad[]> {
    return this.http
      .get<any>(this.contextUrl + 'variedad/search/filter', { params: { 'page': 0, 'size': 500 } }).pipe(map(data => data['_embedded']['variedads']));;
  }

}
