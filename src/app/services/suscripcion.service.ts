// BASE SERVICE
import { Injectable } from '@angular/core';
import { SuscripcionBaseService } from './base/suscripcion.base.service';


// start documentation
/**
 * Custom APIs
 *
 */
// end documentation

/**
 * YOU CAN OVERRIDE HERE SuscripcionBaseService
 */
 @Injectable()
export class SuscripcionService extends SuscripcionBaseService {

}
