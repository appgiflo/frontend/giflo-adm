// BASE SERVICE
import { Injectable } from '@angular/core';
import { CatalogoBaseService } from './base/catalogo.base.service';
import { ParametroBaseService } from './base/parametro.base.service';


// start documentation
/**
 * Custom APIs
 *
 */
// end documentation

/**
 * YOU CAN OVERRIDE HERE CatalogoBaseService
 */
 @Injectable()
export class ParametroService extends ParametroBaseService {

}
