// BASE SERVICE
import { Injectable } from '@angular/core';
import { MessageBaseService } from './base/message.base.service';
import { VariedadBaseService } from './base/variedad.base.service';


// start documentation
/**
 * Custom APIs
 *
 */
// end documentation

/**
 * YOU CAN OVERRIDE HERE VariedadBaseService
 */
 @Injectable()
export class MessageService extends MessageBaseService {

}
