import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalogoListComponent } from './catalogo-list.component';

const routes: Routes = [
  {
    path: '',
    component: CatalogoListComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogoListRoutingModule { }
