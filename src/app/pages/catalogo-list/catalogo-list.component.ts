import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
// Import Services
import { CatalogoService } from '../../services/catalogo.service';
// Import Models
import { Catalogo } from '../../domain/gifloadmin_db/catalogo';
import { MatTableDataSource } from '@angular/material/table';
import { DialogData } from 'src/app/components/dialog/dialog.component';
import { DialogService } from 'src/app/components/dialog.service';
import { ListComponent } from '../base/list-component.service';

// START - USED SERVICES
/**
* CatalogoService.list
*	@description CRUD ACTION list
*	@returns ARRAY OF Catalogo
*
* CatalogoService.delete
*	@description CRUD ACTION delete
*	@param ObjectId id Id Catalogo
*
*/
// END - USED SERVICES

/**
 * This component shows a list of Catalogo
 * @class CatalogoListComponent
 */
@Component({
  selector: 'app-catalogo-list',
  templateUrl: './catalogo-list.component.html',
  styleUrls: ['./catalogo-list.component.scss']
})
export class CatalogoListComponent extends ListComponent implements OnInit {
  list!: Catalogo[];
  grupos!: string[];
  activos!: string[];
  search: any = {};
  idSelected!: string;
  displayedColumns = ["acciones", "codigo", "grupo", "nombre", "fotoNombre", "activo"];

  constructor(
    private catalogoService: CatalogoService,
    private disSer: DialogService
  ) {
    super();
    this.dataSource = new MatTableDataSource();
  }

  /**
   * Init
   */
  ngOnInit(): void {
    this.catalogoService.list().subscribe(map => {
      this.list = map['_embedded']['catalogoes'];
      this.grupos = [];
      this.activos = [];
      this.list.map(c => {
        if (this.grupos.indexOf(c.grupo!) === -1)
          this.grupos.push(c.grupo!);
        if (this.activos.indexOf(c.activo!) === -1)
          this.activos.push(c.activo!);
      });
      this.dataSource = new MatTableDataSource(this.list);
    });
  }
  openConfirm(action: string, id: number) {
    const dialogData: DialogData = { id: id, action: action, msg: 'Desea eliminar el registro' };
    const dialogRef = this.disSer.openDialog(dialogData);
    dialogRef.afterClosed().subscribe(result => {
      if (result.event == 'Delete') {
        this.catalogoService.remove(result.data.id);
      }
    });
  }



}
