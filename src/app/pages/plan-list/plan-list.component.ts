import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
// Import Services
import { PlanService } from '../../services/plan.service';
// Import Models
import { Plan } from '../../domain/gifloadmin_db/plan';
import { ListComponent } from '../base/list-component.service';
import { MatTableDataSource } from '@angular/material/table';
import { DialogService } from 'src/app/components/dialog.service';
import { DialogData } from 'src/app/components/dialog/dialog.component';

// START - USED SERVICES
/**
* PlanService.list
*	@description CRUD ACTION list
*	@returns ARRAY OF Plan
*
* PlanService.delete
*	@description CRUD ACTION delete
*	@param ObjectId id Id Plan
*
*/
// END - USED SERVICES

/**
 * This component shows a list of Plan
 * @class PlanListComponent
 */
@Component({
    selector: 'app-plan-list',
    templateUrl: './plan-list.component.html',
    styleUrls: ['./plan-list.component.scss']
})
export class PlanListComponent extends ListComponent implements OnInit {
    list!: Plan[];
    search: any = {};
    idSelected!: string;
    estados!: string[];
    displayedColumns = ["acciones", "nombre", "valor", "meses", "estado","orden"];
    constructor(
        private planService: PlanService,
        private disSer: DialogService
    ) {
        super();
        this.dataSource = new MatTableDataSource();
    }

    /**
     * Init
     */
    ngOnInit(): void {
        this.planService.list().subscribe(map => {
            this.list = map['_embedded']['plans'];
            this.estados = [];
            this.list.map(c => {
                if (this.estados.indexOf(c.estado!) === -1)
                    this.estados.push(c.estado!);
            });
            this.dataSource = new MatTableDataSource(this.list);
        });
    }

    openConfirm(action: string, id: number) {
        const dialogData: DialogData = { id: id, action: action, msg: 'Desea eliminar el registro' };
        const dialogRef = this.disSer.openDialog(dialogData);
        dialogRef.afterClosed().subscribe(result => {
            if (result.event == 'Delete') {
                this.planService.remove(result.data.id);
            }
        });
    }

}
