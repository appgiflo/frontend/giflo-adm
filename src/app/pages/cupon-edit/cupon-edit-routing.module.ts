import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CuponEditComponent } from './cupon-edit.component';

const routes: Routes = [
  {
    path: '',
    component: CuponEditComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CuponEditRoutingModule { }
