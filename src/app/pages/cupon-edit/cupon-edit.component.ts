// Import Libraries
import { Component, OnInit } from '@angular/core';
import { DatePipe, Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
// Import Services
import { CuponService } from '../../services/cupon.service';
import { PlanService } from '../../services/plan.service';
// Import Models
import { Cupon } from '../../domain/gifloadmin_db/cupon';
import { Plan } from '../../domain/gifloadmin_db/plan';

// START - USED SERVICES
/**
* CuponService.get
*	@description CRUD ACTION get
*	@param ObjectId id Id Cupon
*	@returns Cupon
*
* CuponService.update
*	@description CRUD ACTION update
*	@param ObjectId id Id Cupon
*	@returns Cupon
*
* CuponService.create
*	@description CRUD ACTION create
*	@param Cupon obj Object to insert
*
* PlanService.findBycupon
*	@description CRUD ACTION findBycupon
*	@param Objectid key Id of model to search for
*
*/
// END - USED SERVICES

/**
 * This component allows to edit a Cupon
 */
@Component({
    selector: 'app-cupon-edit',
    templateUrl: 'cupon-edit.component.html',
    styleUrls: ['cupon-edit.component.scss']
})
export class CuponEditComponent implements OnInit {
    item: Cupon;
    planes: Plan[];
    model!: Cupon;
    formValid!: Boolean;

    constructor(
        private cuponService: CuponService,
        private planService: PlanService,
        private route: ActivatedRoute,
        private location: Location) {
        // Init item
        this.item = new Cupon();
        this.planes = [];
    }

    /**
     * Init
     */
    ngOnInit() {
        this.route.params.subscribe(param => {
            const id: string = param['id'];
            if (id !== 'new') {
                this.cuponService.get(id).subscribe(item =>{
                    this.item = item;
                });
            }
            this.planService.list().subscribe(map => {
                this.planes = map['_embedded']['plans'];
            });
            // Get relations
        });
    }


    /**
     * Save Cupon
     *
     * @param {boolean} formValid Form validity check
     * @param Cupon item Cupon to save
     */
    save(formValid: boolean, item: Cupon): void {
        this.formValid = formValid;
        if (formValid) {
            if (item.id) {
                this.cuponService.update(item).subscribe(data => this.goBack());
            } else {
                this.cuponService.create(item).subscribe(data => this.goBack());
            }
        }
    }

    /**
     * Go Back
     */
    goBack(): void {
        this.location.back();
    }


}



