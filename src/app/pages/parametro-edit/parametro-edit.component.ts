// Import Libraries
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Parametro } from 'src/app/domain/gifloadmin_db/parametro';
import { ParametroService } from 'src/app/services/parametro.service';
// START - USED SERVICES
/**
* ParametroService.get
*	@description CRUD ACTION get
*	@param ObjectId id Id Parametro
*	@returns Parametro
*
* ParametroService.update
*	@description CRUD ACTION update
*	@param ObjectId id Id Parametro
*	@returns Parametro
*
* ParametroService.create
*	@description CRUD ACTION create
*	@param Parametro obj Object to insert
*
*/
// END - USED SERVICES

/**
 * This component allows to edit a Parametro
 */
@Component({
    selector: 'app-parametro-edit',
    templateUrl: 'parametro-edit.component.html',
    styleUrls: ['parametro-edit.component.scss']
})
export class ParametroEditComponent implements OnInit {

    item!: Parametro;
    model!: Parametro;
    formValid!: Boolean;
    isNew!:boolean;

    constructor(
        private ParametroService: ParametroService,
        private route: ActivatedRoute,
        private location: Location) {
        // Init item
        this.item = new Parametro();
    }

    /**
     * Init
     */
    ngOnInit() {
        this.route.params.subscribe(param => {
            const id: string = param['id'];
            this.isNew=id === 'new';
            if (id !== 'new') {
                this.ParametroService.get(id).subscribe(item => this.item = item);
            }
            // Get relations
        });
    }


    /**
     * Save Parametro
     *
     * @param {boolean} formValid Form validity check
     * @param Parametro item Parametro to save
     */
    save(formValid: boolean, item: Parametro): void {
        this.formValid = formValid;
        if (formValid) {
            if (!this.isNew) {
                this.ParametroService.update(item).subscribe(data => this.goBack());
            } else {
                this.ParametroService.create(item).subscribe(data => this.goBack());
            }
        }
    }

    /**
     * Go Back
     */
    goBack(): void {
        this.location.back();
    }


}



