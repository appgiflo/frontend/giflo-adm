import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ParametroEditComponent } from './parametro-edit.component';

const routes: Routes = [
  {
    path: '',
    component: ParametroEditComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParametroEditRoutingModule { }
