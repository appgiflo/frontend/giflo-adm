// Import Libraries
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
// Import Services
import { VariedadService } from '../../services/variedad.service';
// Import Models
import { Variedad } from '../../domain/gifloadmin_db/variedad';
import { CatalogoService } from 'src/app/services/catalogo.service';
import { List } from 'lodash';
import { Catalogo } from 'src/app/domain/gifloadmin_db/catalogo';

// START - USED SERVICES
/**
* VariedadService.get
*	@description CRUD ACTION get
*	@param ObjectId id Id Variedad
*	@returns Variedad
*
* VariedadService.update
*	@description CRUD ACTION update
*	@param ObjectId id Id Variedad
*	@returns Variedad
*
* VariedadService.create
*	@description CRUD ACTION create
*	@param Variedad obj Object to insert
*
*/
// END - USED SERVICES

/**
 * This component allows to edit a Variedad
 */
@Component({
  selector: 'app-variedad-edit',
  templateUrl: 'variedad-edit.component.html',
  styleUrls: ['variedad-edit.component.scss']
})
export class VariedadEditComponent implements OnInit {
  item: Variedad;
  model: Variedad = new Variedad;
  formValid!: Boolean;
  colores!: Catalogo[];
  constructor(
    private variedadService: VariedadService,
    private catalogoService: CatalogoService,
    private route: ActivatedRoute,
    private location: Location) {
    // Init item
    this.item = new Variedad();

  }

  /**
   * Init
   */
  async ngOnInit() {
    this.colores = await this.catalogoService.listByGroup().toPromise();
    this.route.params.subscribe(param => {
      const id: string = param['id'];
      if (id !== 'new') {
        this.variedadService.get(id).subscribe(item => this.item = item);
      }
      // Get relations
    });
  }


  /**
   * Save Variedad
   *
   * @param {boolean} formValid Form validity check
   * @param Variedad item Variedad to save
   */
  save(formValid: boolean, item: Variedad): void {
    this.formValid = formValid;
    if (formValid) {
      this.variedadService.save(item).subscribe(data => this.goBack());
    }
  }

  /**
   * Go Back
   */
  goBack(): void {
    this.location.back();
  }


}



