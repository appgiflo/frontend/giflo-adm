import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
// Import Services
import { VariedadService } from '../../services/variedad.service';
// Import Models
import { Variedad } from '../../domain/gifloadmin_db/variedad';
import { ListComponent } from '../base/list-component.service';
import { MatTableDataSource } from '@angular/material/table';
import { DialogService } from 'src/app/components/dialog.service';
import { DialogData } from 'src/app/components/dialog/dialog.component';
import { Catalogo } from 'src/app/domain/gifloadmin_db/catalogo';
import { CatalogoService } from 'src/app/services/catalogo.service';
import { List } from 'lodash';

// START - USED SERVICES
/**
* VariedadService.list
*	@description CRUD ACTION list
*	@returns ARRAY OF Variedad
*
* VariedadService.delete
*	@description CRUD ACTION delete
*	@param ObjectId id Id Variedad
*
*/
// END - USED SERVICES

/**
 * This component shows a list of Variedad
 * @class VariedadListComponent
 */
@Component({
  selector: 'app-variedad-list',
  templateUrl: './variedad-list.component.html',
  styleUrls: ['./variedad-list.component.scss']
})
export class VariedadListComponent extends ListComponent implements OnInit {
  list: Variedad[] = [];
  search: any = {};
  displayedColumns = ["acciones", "nombreColor", "nombre",  "activo"];
  colores!: Catalogo[];
  constructor(
    private variedadService: VariedadService,
    private catalogoService: CatalogoService,
    private disSer: DialogService
  ) {
    super();
    this.dataSource = new MatTableDataSource();
  }

  /**
   * Init
   */
  async ngOnInit() {
    this.colores = await this.catalogoService.listByGroup().toPromise();
    this.variedadService.list().subscribe(data => {
      this.list=data;
      this.dataSource = new MatTableDataSource(this.list);
    });
  }

  openConfirm(action: string, id: number) {
    const dialogData: DialogData = { id: id, action: action, msg: 'Desea eliminar el registro' };
    const dialogRef = this.disSer.openDialog(dialogData);
    dialogRef.afterClosed().subscribe(result => {
      if (result.event == 'Delete') {
        this.variedadService.remove(result.data.id);
      }
    });
  }

}
