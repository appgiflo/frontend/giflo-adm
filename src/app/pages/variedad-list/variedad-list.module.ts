import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VariedadListComponent } from './variedad-list.component';
import { VariedadListRoutingModule } from './variedad-list-routing.module';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';


@NgModule({
  imports: [
    CommonModule,
    VariedadListRoutingModule,
    FormsModule,
    SharedModule,
    MatCardModule,
    MatFormFieldModule,
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    MatIconModule,
    MatPaginatorModule,
    MatInputModule,
    FlexLayoutModule
  ],
  declarations: [
    VariedadListComponent
  ]
})
export class VariedadListModule { }
