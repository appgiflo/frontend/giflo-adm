import { Injectable, ViewChild, AfterViewInit, Component } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-list',
  template:''
})
export class ListComponent implements AfterViewInit {

  displayedColumns = ['id', 'nombre', 'estado'];
  dataSource!: MatTableDataSource<any>;
  @ViewChild(MatPaginator, {}) paginator!: MatPaginator;
  @ViewChild(MatSort, {}) sort!: MatSort;
  constructor() { }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  applyFilter(filterValue: string) {
    filterValue='Todos'==filterValue?'':filterValue;
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
}
