import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlanEditComponent } from './plan-edit.component';

const routes: Routes = [
  {
    path: '',
    component: PlanEditComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanEditRoutingModule { }
