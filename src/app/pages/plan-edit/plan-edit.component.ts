// Import Libraries
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
// Import Services
import { PlanService } from '../../services/plan.service';
import { CuponService } from '../../services/cupon.service';
// Import Models
import { Plan } from '../../domain/gifloadmin_db/plan';
import { Cupon } from '../../domain/gifloadmin_db/cupon';

// START - USED SERVICES
/**
* PlanService.get
*	@description CRUD ACTION get
*	@param ObjectId id Id Plan
*	@returns Plan
*
* PlanService.update
*	@description CRUD ACTION update
*	@param ObjectId id Id Plan
*	@returns Plan
*
* PlanService.create
*	@description CRUD ACTION create
*	@param Plan obj Object to insert
*
* CuponService.list
*	@description CRUD ACTION list
*	@returns ARRAY OF Cupon
*
*/
// END - USED SERVICES

/**
 * This component allows to edit a Plan
 */
@Component({
    selector: 'app-plan-edit',
    templateUrl: 'plan-edit.component.html',
    styleUrls: ['plan-edit.component.scss']
})
export class PlanEditComponent implements OnInit {
    item: Plan;
    model!: Plan;
    formValid!: Boolean;
    colorPalete: string;

    constructor(
        private planService: PlanService,
        private cuponService: CuponService,
        private route: ActivatedRoute,
        private location: Location) {
        // Init item
        this.item = new Plan();
        this.colorPalete = "";
    }

    /**
     * Init
     */
    ngOnInit() {
        this.route.params.subscribe(param => {
            const id: string = param['id'];
            if (id !== 'new') {
                this.planService.get(id).subscribe(item => this.item = item);
            }
            // Get relations
        });

    }


    /**
     * Save Plan
     *
     * @param {boolean} formValid Form validity check
     * @param Plan item Plan to save
     */
    save(formValid: boolean, item: Plan): void {

        this.formValid = formValid;
        if (formValid) {
            if (item.id) {
                this.planService.update(item).subscribe(data => this.goBack());
            } else {
                this.planService.create(item).subscribe(data => this.goBack());
            }
        }
    }

    /**
     * Go Back
     */
    goBack(): void {
        this.location.back();
    }
    changedPrimary(e: any) {
        let color: string = e.target.value;
        color='0xff'+color.slice(1,7);
        this.item.colorPrimario=color;

    }
    changedSecundario(e: any) {
        let color: string = e.target.value;
        color='0xff'+color.slice(1,7);
        this.item.colorSecundario=color;
    }
    changedTexto(e: any) {
        let color: string = e.target.value;
        color='0xff'+color.slice(1,7);
        this.item.colorTexto=color;
    }


}



