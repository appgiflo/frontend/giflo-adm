import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuscripcionListComponent } from './suscripcion-list.component';
import { SuscripcionListRoutingModule } from './suscripcion-list-routing.module';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatSortModule } from '@angular/material/sort';
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";

@NgModule({
  imports: [
    CommonModule,
    SuscripcionListRoutingModule,
    FormsModule,
    SharedModule,
    MatCardModule,
    MatFormFieldModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatPaginatorModule,
    MatInputModule,
    FlexLayoutModule,
    MatSortModule,
    MatProgressSpinnerModule
    
  ],
  declarations: [
    SuscripcionListComponent
  ]
})
export class SuscripcionListModule { }
