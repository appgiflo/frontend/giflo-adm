import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { BehaviorSubject, Observable, of } from "rxjs";
import { catchError, finalize } from "rxjs/operators";
import { Suscripcion } from "src/app/domain/gifloadmin_db/suscripcion";
import { SuscripcionService } from "src/app/services/suscripcion.service";

export class SuscripcionDataSource implements DataSource<Suscripcion> {

    private lessonsSubject = new BehaviorSubject<Suscripcion[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();
    constructor(private suscripcionService: SuscripcionService) { }

    connect(collectionViewer: CollectionViewer): Observable<Suscripcion[]> {
        return this.lessonsSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.lessonsSubject.complete();
        this.loadingSubject.complete();
    }

    loadSuscripciones(filter = '', codigo: string, sortDirection = 'asc', pageIndex: number, pageSize: number) {
        this.loadingSubject.next(true);

        this.suscripcionService.list(filter, codigo, sortDirection,
            pageIndex, pageSize).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe(lessons => this.lessonsSubject.next(lessons));
    }
}