import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SuscripcionListComponent } from './suscripcion-list.component';

const routes: Routes = [
  {
    path: '',
    component: SuscripcionListComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuscripcionListRoutingModule { }
