import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { OnInit } from '@angular/core';
// Import Services
import { SuscripcionService } from '../../services/suscripcion.service';
// Import Models
import { Suscripcion } from '../../domain/gifloadmin_db/suscripcion';
import { ListComponent } from '../base/list-component.service';
import { MatTableDataSource } from '@angular/material/table';
import { DialogData } from 'src/app/components/dialog/dialog.component';
import { DialogService } from 'src/app/components/dialog.service';
import { SuscripcionDataSource } from './suscripcion-data.source';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { fromEvent, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';

// START - USED SERVICES
/**
* SuscripcionService.list
*	@description CRUD ACTION list
*	@returns ARRAY OF Suscripcion
*
* SuscripcionService.delete
*	@description CRUD ACTION delete
*	@param ObjectId id Id
*
*/
// END - USED SERVICES

/**
 * This component shows a list of Suscripcion
 * @class SuscripcionListComponent
 */
@Component({
    selector: 'app-suscripcion-list',
    templateUrl: './suscripcion-list.component.html',
    styleUrls: ['./suscripcion-list.component.scss']
})
export class SuscripcionListComponent implements OnInit, AfterViewInit {

    list!: Suscripcion[];
    search: any = {};
    displayedColumns = ["acciones", "usuario", "codigoPago", "documento", "fechaPago", "estado", "cupon", "total"];
    dataSourcePage!: SuscripcionDataSource;

    @ViewChild(MatPaginator) paginator!: MatPaginator;
    @ViewChild(MatSort) sort!: MatSort;
    @ViewChild('filter') filter!: ElementRef;
    @ViewChild('codigo') codigo!: ElementRef;
    constructor(
        private route: ActivatedRoute,
        private suscripcionService: SuscripcionService,
        private disSer: DialogService,
    ) { }

    /**
     * Init
     */
    ngOnInit(): void {
        this.search = this.route.snapshot.data["course"];
        this.dataSourcePage = new SuscripcionDataSource(this.suscripcionService);
        this.dataSourcePage.loadSuscripciones('', '', '', 0, 30);
    }
    ngAfterViewInit() {
        // server-side search
        fromEvent(this.filter.nativeElement, 'keyup')
            .pipe(
                debounceTime(500),
                distinctUntilChanged(),
                tap(() => {
                    this.paginator.pageIndex = 0;
                    this.loadLessonsPage();
                })
            )
            .subscribe();
        fromEvent(this.codigo.nativeElement, 'keyup')
            .pipe(
                debounceTime(500),
                distinctUntilChanged(),
                tap(() => {
                    this.paginator.pageIndex = 0;
                    this.loadLessonsPage();
                })
            )
            .subscribe();

        // reset the paginator after sorting
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        // on sort or paginate events, load a new page
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => this.loadLessonsPage())
            )
            .subscribe();
    }

    loadLessonsPage() {
        this.dataSourcePage.loadSuscripciones(
            this.filter.nativeElement.value,
            this.codigo.nativeElement.value,
            this.sort.direction,
            this.paginator.pageIndex,
            this.paginator.pageSize);
    }


}
