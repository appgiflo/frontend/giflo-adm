import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalogoEditComponent } from './catalogo-edit.component';

const routes: Routes = [
  {
    path: '',
    component: CatalogoEditComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogoEditRoutingModule { }
