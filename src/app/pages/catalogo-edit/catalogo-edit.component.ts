// Import Libraries
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
// Import Services
import { CatalogoService } from '../../services/catalogo.service';
// Import Models
import { Catalogo } from '../../domain/gifloadmin_db/catalogo';

// START - USED SERVICES
/**
* CatalogoService.get
*	@description CRUD ACTION get
*	@param ObjectId id Id Catalogo
*	@returns Catalogo
*
* CatalogoService.update
*	@description CRUD ACTION update
*	@param ObjectId id Id Catalogo
*	@returns Catalogo
*
* CatalogoService.create
*	@description CRUD ACTION create
*	@param Catalogo obj Object to insert
*
*/
// END - USED SERVICES

/**
 * This component allows to edit a Catalogo
 */
@Component({
    selector: 'app-catalogo-edit',
    templateUrl: 'catalogo-edit.component.html',
    styleUrls: ['catalogo-edit.component.scss']
})
export class CatalogoEditComponent implements OnInit {

    item!: Catalogo;
    model!: Catalogo;
    formValid!: Boolean;
    activo!: Boolean;

    constructor(
        private catalogoService: CatalogoService,
        private route: ActivatedRoute,
        private location: Location) {
        // Init item
        this.item = new Catalogo();
    }

    /**
     * Init
     */
    ngOnInit() {
        this.route.params.subscribe(param => {
            const id: string = param['id'];
            if (id !== 'new') {
                this.catalogoService.get(id).subscribe(item => { this.item = item; this.activo = this.item.activo == 'SI' });
            } else {
                this.activo = false;
            }
            // Get relations
        });
    }


    /**
     * Save Catalogo
     *
     * @param {boolean} formValid Form validity check
     * @param Catalogo item Catalogo to save
     */
    save(formValid: boolean, item: Catalogo): void {
        this.formValid = formValid;
        if (formValid) {
            item.activo = this.activo ? "SI" : "NO";
            if (item.id) {
                this.catalogoService.update(item).subscribe(data => this.goBack());
            } else {
                this.catalogoService.create(item).subscribe(data => this.goBack());
            }
        }
    }

    /**
     * Go Back
     */
    goBack(): void {
        this.location.back();
    }


}



