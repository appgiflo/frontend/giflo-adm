// Import Libraries
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { MessageFirebase } from 'src/app/domain/gifloadmin_db/message.firebase';
import { MessageService } from 'src/app/services/message.service';
import { UserService } from '../../services/user.service';

// START - USED SERVICES
/**
* SuscripcionService.get
*	@description CRUD ACTION get
*	@param ObjectId id Id Suscripcion
*	@returns Suscripcion
*
* SuscripcionService.update
*	@description CRUD ACTION update
*	@param ObjectId id Id Suscripcion
*	@returns Suscripcion
*
* SuscripcionService.create
*	@description CRUD ACTION create
*
* UserService.findBysuscripciones
*	@description CRUD ACTION findBysuscripciones
*	@param Objectid key Id of model to search for
*
*/
// END - USED SERVICES

/**
 * This component allows to edit a Suscripcion
 */
@Component({
    selector: 'app-message-edit',
    templateUrl: 'message-edit.component.html',
    styleUrls: ['message-edit.component.scss']
})
export class MessageEditComponent implements OnInit {
    item: MessageFirebase;
    model!: MessageFirebase;
    formValid!: Boolean;

    constructor(
        private messageService: MessageService, private _snackBar: MatSnackBar) {
        this.item = new MessageFirebase();
    }

    /**
     * Init
     */
    ngOnInit() {
        console.log('init');
    }

    /**
     * Save Suscripcion
     *
     * @param {boolean} formValid Form validity check
     * @param Suscripcion item Suscripcion to save
     */
    send(formValid: boolean, item: MessageFirebase): void {
        this.formValid = formValid;
        if (formValid) {
            this.messageService.send(item).then(data => {
                this._snackBar.open(data.title!, 'Aceptar');
            }).catch((e) => {
                console.log(e.message);
                this._snackBar.open(e.message, 'Aceptar');
            });
        } else {
            this._snackBar.open('Revise los campos obligatorios!', 'Aceptar');
        }
    }
}



