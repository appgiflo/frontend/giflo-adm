import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MessageEditComponent } from './message-edit.component';

const routes: Routes = [
  {
    path: '',
    component: MessageEditComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessageEditRoutingModule { }
