// Import Libraries
import { Component } from '@angular/core';
import { Location } from '@angular/common';

// Import Services

// START - USED SERVICES

// END - USED SERVICES

/**
 * Home Component
 */
@Component({
    selector: 'app-home',
    templateUrl : './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent {
    title = 'Giflo Admin';

    constructor(
        private location: Location
        ) {

    }
}
