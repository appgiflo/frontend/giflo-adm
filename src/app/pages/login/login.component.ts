import { Component } from '@angular/core';
import { User } from 'src/app/domain/gifloadmin_db/user';
import { NgForm } from '@angular/forms';
import { AuthenticationService } from 'src/app/security/authentication.service';
import { Router } from '@angular/router';
import { store } from 'src/app/security/current-user';
import { SecurityService } from 'src/app/security/services/security.service';

import { AESService } from 'src/app/security/aes.service';
import { NotificadorAppService } from 'src/app/security/services/notificador.app.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  user: User = new User(-1, '', '', '', []);
  showError: boolean = false;
  remember: boolean = false;


  constructor(
    private securityService: SecurityService,
    private aes: AESService,
    private router: Router,
    private notification: NotificadorAppService
  ) { }

  login(form: NgForm) {
    if (form.valid) {
      this.notification.show('Ingresando...');
      var userEncrypted = this.aes.encryptUsingAES256(this.user.correo);
      this.securityService.login(userEncrypted, this.user.contrasenia, this.remember === undefined ? false : this.remember)
        .subscribe(
          user => {
            this.showError = false;
            this.notification.hide();
            this.router.navigate(['/']);
            this.setUser(user);
          },
          err =>{
            this.notification.hide();
            this.showError = true;
          }
        );
    }
  }
  private setUser(user: User) {
    store.setUser(user);
  }
}
