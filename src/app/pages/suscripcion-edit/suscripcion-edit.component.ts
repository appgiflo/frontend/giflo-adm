// Import Libraries
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
// Import Services
import { SuscripcionService } from '../../services/suscripcion.service';
import { UserService } from '../../services/user.service';
// Import Models
import { Suscripcion } from '../../domain/gifloadmin_db/suscripcion';
import { User } from '../../domain/gifloadmin_db/user';

// START - USED SERVICES
/**
* SuscripcionService.get
*	@description CRUD ACTION get
*	@param ObjectId id Id Suscripcion
*	@returns Suscripcion
*
* SuscripcionService.update
*	@description CRUD ACTION update
*	@param ObjectId id Id Suscripcion
*	@returns Suscripcion
*
* SuscripcionService.create
*	@description CRUD ACTION create
*
* UserService.findBysuscripciones
*	@description CRUD ACTION findBysuscripciones
*	@param Objectid key Id of model to search for
*
*/
// END - USED SERVICES

/**
 * This component allows to edit a Suscripcion
 */
@Component({
    selector: 'app-suscripcion-edit',
    templateUrl: 'suscripcion-edit.component.html',
    styleUrls: ['suscripcion-edit.component.scss']
})
export class SuscripcionEditComponent implements OnInit {
    item: Suscripcion;
    externalUser_suscripciones: User[];
    model!: Suscripcion;
    formValid!: Boolean;

    constructor(
        private suscripcionService: SuscripcionService,
        private userService: UserService,
        private route: ActivatedRoute,
        private location: Location) {
        // Init item
        this.item = new Suscripcion();
        this.externalUser_suscripciones = [];
    }

    /**
     * Init
     */
    ngOnInit() {
        this.route.params.subscribe(param => {
            const id: string = param['id'];
            if (id !== 'new') {
                this.suscripcionService.get(id).subscribe(item => this.item = item);
            }
            // Get relations
        });
    }


    /**
     * Save Suscripcion
     *
     * @param {boolean} formValid Form validity check
     * @param Suscripcion item Suscripcion to save
     */
    save(formValid: boolean, item: Suscripcion): void {
        this.formValid = formValid;
        if (formValid) {
            this.suscripcionService.update(item).subscribe(data => this.goBack());
        }
    }

    /**
     * Go Back
     */
    goBack(): void {
        this.location.back();
    }


}



