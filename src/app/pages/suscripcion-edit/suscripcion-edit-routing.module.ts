import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SuscripcionEditComponent } from './suscripcion-edit.component';

const routes: Routes = [
  {
    path: '',
    component: SuscripcionEditComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuscripcionEditRoutingModule { }
