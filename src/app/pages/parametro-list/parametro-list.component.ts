import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { DialogService } from 'src/app/components/dialog.service';
import { DialogData } from 'src/app/components/dialog/dialog.component';
import { ParametroService } from 'src/app/services/parametro.service';
// Import Models
import { Catalogo } from '../../domain/gifloadmin_db/catalogo';
import { ListComponent } from '../base/list-component.service';

// START - USED SERVICES
/**
* CatalogoService.list
*	@description CRUD ACTION list
*	@returns ARRAY OF Catalogo
*
* CatalogoService.delete
*	@description CRUD ACTION delete
*	@param ObjectId id Id Catalogo
*
*/
// END - USED SERVICES

/**
 * This component shows a list of Catalogo
 * @class ParametroListComponent
 */
@Component({
  selector: 'app-parametro-list',
  templateUrl: './parametro-list.component.html',
  styleUrls: ['./parametro-list.component.scss']
})
export class ParametroListComponent extends ListComponent implements OnInit {
  list!: Catalogo[];
  search: any = {};
  idSelected!: string;
  displayedColumns = ["acciones", "id", "descripcion", "valor", "activo"];

  constructor(
    private catalogoService: ParametroService,
    private disSer: DialogService
  ) {
    super();
    this.dataSource = new MatTableDataSource();
  }

  /**
   * Init
   */
  ngOnInit(): void {
    this.catalogoService.list().subscribe(map => {
      this.list = map;
      this.dataSource = new MatTableDataSource(this.list);
    });
  }
  openConfirm(action: string, id: number) {
    const dialogData: DialogData = { id: id, action: action, msg: 'Desea eliminar el registro' };
    const dialogRef = this.disSer.openDialog(dialogData);
    dialogRef.afterClosed().subscribe(result => {
      if (result.event == 'Delete') {
        this.catalogoService.remove(result.data.id);
      }
    });
  }



}
