import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ParametroListComponent } from './parametro-list.component';

const routes: Routes = [
  {
    path: '',
    component: ParametroListComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParametroListRoutingModule { }
