import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CuponListComponent } from './cupon-list.component';

const routes: Routes = [
  {
    path: '',
    component: CuponListComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CuponListRoutingModule { }
