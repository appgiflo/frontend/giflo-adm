import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
// Import Services
import { CuponService } from '../../services/cupon.service';
// Import Models
import { Cupon } from '../../domain/gifloadmin_db/cupon';
import { ListComponent } from '../base/list-component.service';
import { MatTableDataSource } from '@angular/material/table';
import { DialogData } from 'src/app/components/dialog/dialog.component';
import { DialogService } from 'src/app/components/dialog.service';
import { PlanService } from 'src/app/services/plan.service';
import { Plan } from 'src/app/domain/gifloadmin_db/plan';

// START - USED SERVICES
/**
* CuponService.list
*	@description CRUD ACTION list
*	@returns ARRAY OF Cupon
*
* CuponService.delete
*	@description CRUD ACTION delete
*	@param ObjectId id Id Cupon
*
*/
// END - USED SERVICES

/**
 * This component shows a list of Cupon
 * @class CuponListComponent
 */
@Component({
    selector: 'app-cupon-list',
    templateUrl: './cupon-list.component.html',
    styleUrls: ['./cupon-list.component.scss']
})
export class CuponListComponent extends ListComponent implements OnInit {
    list!: Cupon[];
    search: any = {};
    idSelected!: string;
    estados!: string[];
    planes!: Plan[];
    displayedColumns = ["acciones", "plan", "codigo", "fechaInicio", "fechaFin", "porciento", "mensaje", "estado"];
    constructor(
        private cuponService: CuponService,
        private disSer: DialogService,
        private planService: PlanService
    ) {
        super();
        this.dataSource = new MatTableDataSource();
    }

    /**
     * Init
     */
    ngOnInit(): void {
        this.cuponService.list().subscribe(map => {
            this.list = map['_embedded']['cupons'];
            this.planes = [];
            this.estados = [];

            this.dataSource = new MatTableDataSource(this.list);
            this.planService.list().subscribe(map => {
                this.planes = map['_embedded']['plans'];
                this.list.map(c => {
                    if (this.estados.indexOf(c.estado!) === -1)
                        this.estados.push(c.estado!);
                    this.list.map(cu => { 
                        var plan=this.planes.find(p=>p.id===cu.idPlan);
                        if(plan){
                            cu.plan=plan.nombre;
                        }
                    });
                });
            });
        });
    }
    openConfirm(action: string, id: number) {
        const dialogData: DialogData = { id: id, action: action, msg: 'Desea eliminar el registro' };
        const dialogRef = this.disSer.openDialog(dialogData);
        dialogRef.afterClosed().subscribe(result => {
            if (result.event == 'Delete') {
                this.cuponService.remove(result.data.id);
            }
        });
    }


}
