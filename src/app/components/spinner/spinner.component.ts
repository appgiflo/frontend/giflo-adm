import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {
  Router,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError
} from '@angular/router';
import { NotificadorAppService } from 'src/app/security/services/notificador.app.service';
@Component({
  selector: 'app-spinner',
  template: `<div class="preloader" *ngIf="isSpinnerVisible">
      <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>

      </div>
      <div style="text-align: center;">{{texto}}</div>
    </div>`,
  encapsulation: ViewEncapsulation.None,
  styleUrls: []
})
export class SpinnerComponent implements OnInit {
  public isSpinnerVisible = true;
  public texto = '';
  constructor(private router: Router, private _NotificadorApp: NotificadorAppService) {
    this._NotificadorApp.spinnerObs$.subscribe((DataSpinner: { show: boolean; text?: string }) => {
      if (DataSpinner != null) {
        this.isSpinnerVisible = DataSpinner.show;
        this.texto = DataSpinner.text! > '' ? DataSpinner.text! : 'Procesando';
      }
    });
    this.router.events.subscribe(
      event => {
        if (event instanceof NavigationStart) {
          this.isSpinnerVisible = true;
        } else if (
          event instanceof NavigationEnd ||
          event instanceof NavigationCancel ||
          event instanceof NavigationError
        ) {
          this.isSpinnerVisible = false;
        }
      },
      () => {
        this.isSpinnerVisible = false;
      }
    );
  }

  ngOnInit() {
    this.isSpinnerVisible = false;
  }

}
