import { Component, EventEmitter, Output } from '@angular/core';
import { store } from 'src/app/security/current-user';
import { OnInit } from '@angular/core';
import { User } from 'src/app/domain/gifloadmin_db/user';
import { AuthenticationService } from 'src/app/security/authentication.service';

/**
 * This component manage the NavBar
 *
 * @class NavbarComponent
 */
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
})
export class NavbarComponent implements OnInit {
  @Output() public toggleSidenav = new EventEmitter();
  public user?: User;
  constructor(public authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.authenticationService.getUser().subscribe(user => this.user = user, err => this.user = new User(-1, '', '', '', []));
    store.currentUser$.subscribe(user => this.user = user);
  }
}
